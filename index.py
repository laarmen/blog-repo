def buildIndex(post):
    if post.status == DRAFT:
        prepare_draft(post)
    else:
        prepare_published(post)

    buildIndexHelper(post)

def buildIndexHelper(post):
    doNormalStuff(post)
    # Handle the Carroll special case in its own thread since it is heavy
    # computation.
    if post.status == DELETED and post.author.startswith("Lewis"):
        spawn(lambda: goDownTheRabbitHole(post.clone()))
    doMoreNormalStuff(post)
